
#!/bin/sh
#
# Base ArchLinux install script for encrypted LVM 
# WARRNING !!! /boot partition is not encrypted cuz of no compatibility
# grub with luks2. Ill add some mitigation for this problem in the future
# if u have some good idea for this problem please contact me at nconfig@proton.me 

MAPPERDEV=cryptroot
LVMGROUP=vg0

ROOTSIZE=35G
VARSIZE=10G
SWAPSIZE=2G

FILESYS=ext4	# please don't change. support for other fs will be avaible

echo -e " this is alpha version of script and im still learning. \n PLEASE DONT BLAME ME IF SOMETHING DOESN'T WORK \n\n"
lsblk
read -p "Please enter name of drive (np. sda ; nvme0n1): " DRIVE
echo "Prepare to partition disk (for now there is no automatic partitioning
make sure that there are 2 partitions (one for EFI and other for encrypted lvm)"
sleep 5
cfdisk /dev/$DRIVE
read -p "Name of EFI partition: " EFIPART
read -p "Name of encrypted LVM partition: " LVMPART
mkfs.fat -F32 /dev/$EFIPART

# Encrypt drive
echo "Now enter password to ENCRYPTED disk twice"
cryptsetup -v -c aes-xts-plain64 -s 512 -h sha512 -i 500 luksFormat /dev/$LVMPART
echo "Enter your ENCRYPTED disk one more time to setup system"
cryptsetup luksOpen /dev/$LVMPART $MAPPERDEV

# Setup LVM
pvcreate /dev/mapper/$MAPPERDEV
vgcreate $LVMGROUP /dev/mapper/$MAPPERDEV
lvcreate -L $ROOTSIZE $LVMGROUP -n root
lvcreate -L $VARSIZE $LVMGROUP -n var
lvcreate -L $SWAPSIZE $LVMGROUP -n swap
lvcreate -l 100%FREE $LVMGROUP -n home

for fsys in home var root ; do
	mkfs.ext4 /dev/$LVMGROUP/$fsys
done

mount /dev/$LVMGROUP/root /mnt
mkdir /mnt/{boot,var,home}
mount /dev/$LVMGROUP/var /mnt/var
mount /dev/$LVMGROUP/home /mnt/home
mount /dev/$EFIPART /mnt/boot
lsblk

# Install packages
pacstrap /mnt linux-hardened linux-firmware base apparmor networkmanager doas lvm2 grub efibootmgr
genfstab /mnt >> /mnt/etc/fstab

echo 'GRUB_DEFAULT=0
GRUB_TIMEOUT=0
GRUB_DISTRIBUTOR="Arch"
GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 cryptdevice=/dev/TOCHANGELVM:TOCHANGEGRP lsm=apparmor,lockdown,yama apparmor=1 init_on_alloc=1 init_on_free=1 l1tf=full,force l1d_flush=on page_alloc.shuffle=1 slab_nomerge slub_debug=F spec_store_bypass_disable=on spectre_v2=on vsyscall=none randomize_kstack_offset=1
GRUB_CMDLINE_LINUX=""
GRUB_PRELOAD_MODULES="part_gpt part_msdos"
GRUB_TIMEOUT_STYLE=menu
GRUB_TERMINAL_INPUT=console
GRUB_GFXMODE=auto
GRUB_GFXPAYLOAD_LINUX=keep
GRUB_DISABLE_RECOVERY=true
GRUB_COLOR_NORMAL="light-blue/black"
GRUB_COLOR_HIGHLIGHT="light-cyan/blue"
#GRUB_BACKGROUND="/path/to/wallpaper"
#GRUB_THEME="/path/to/gfxtheme"
GRUB_DISABLE_SUBMENU=y
#GRUB_DISABLE_OS_PROBER=false' | sed -e s/TOCHANGELVM/$LVMPART/g -e s/TOCHANGEGRP/$LVMGROUP/g > tmpgrub
rm /mnt/etc/default/grub
mv tmpgrub /mnt/etc/default/grub

chmod 744 archpost.sh
chmod 744 archsetup.sh && mv archsetup.sh /mnt/.
arch-chroot /mnt ./archsetup.sh
./archpost.sh

