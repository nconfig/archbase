#!/bin/bash
#
# pt.2 of archinstall script to setup base desktop
# default variable options
HOSTNAME=host

# install dnscrypt-proxy to resolv encrypted dns
pacman -Syu --noconfirm dnscrypt-proxy && systemctl enable --now dnscrypt-proxy
echo "nameserver 127.0.0.1" > /etc/resolv.conf
chattr +i /etc/resolv.conf 	# set to not overwrite changes

# install openntpd daemon to sync time
pacman -Syu --noconfirm openntpd && systemctl enable openntpd
systemctl disable --now systemd-timesyncd
systemctl start openntpd

# startup impotrant services
systemctl enable NetworkManager
systemctl enable apparmor

# install basic desktop and services
pacman -Syu --noconfirm git zsh exo garcon thunar thunar-volman tumbler xfce4-appfinder xfce4-panel \
	xfc4-power-manager xfce4-session xfce4-settings xfce4-terminal xfconf xfdesktop xfwm4 \
	xfwm4-themes xfce4-datetime-plugin xfce-pulseaudio-plugin xfce4-screenshooter \
	xfce4-sensors-plugin xfce4-screensaver xfce4-systemload-plugin xfce4-weather-plugin

pacman -S --noconfirm sddm && systemctl enable sddm
pacman -S --noconfirm pipewire pipewire-pulse pipewire-media-session pavucontrol

# setup admin user read -p "Name of ur user: " USERNAME
read -p "Enter ur username: "
useradd -mG wheel $USERNAME
usermod -aG video $USERNAME
usermod -aG sound $USERNAME
usermod -aG cdrom $USERNAME
usermod -aG input $USERNAME
usermod -aG proc $USERNAME
usermod --shell /bin/zsh $USERNAME
echo "Type user's password (np. Ididnot+like!Horses-123)"
passwd $USERNAME

# install basic bloat
pacman -S --noconfirm go vlc base-devel

# enable AUR
mkdir /home/$USERNAME/.makepkg cd /home/$USERNAME/.makepkg
git clone https://aur.archlinux.org/yay-git.git && cd yay-git
makepkg && pacman -U --noconfirm yay-*
sudo -u $USERNAME yay -Sy --noconfirm librewolf-bin

cd /

# select hostname
read -p "enter name of ur PC (choose wisely, if this pc will be in public network, admin can see this name): " HOSTNAME
echo "$HOSTNAME" > /etc/hostname
echo -e "127.0.0.1 localhost \n::1 localhost \n127.0.1.1 $HOSTNAME.localdomain $HOSTNAME" > /etc/hosts

# select language and config locale
read -p "enter ur language (en or pl)" LANG
if [[ $LANG == pl ]] ; then
	sed -i s/^.pl_PL/pl_PL/g /etc/locale.gen
	echo 'LANG="pl_PL.UTF-8 UTF-8"' > /etc/locale.conf
else
	sed -i s/^.en_US/en_US/g /etc/locale.gen
	echo 'LANG="en_US.UTF-8 UTF-8"' > /etc/locale.conf
fi
locale-gen

# setup "sudo" (doas)
echo "permit persist :wheel" > tmpdoas
mv tmpdoas /etc/doas.conf
ln -sf /usr/bin/doas /usr/bin/sudo

# setup grub (bootloader)
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Grub
grub-mkconfig -o /boot/grub/grub.cfg

# setup .zshrc
git clone https://gitlab.com/nconfig/dotfiles
mv .config/zsh/.zshrc /home/$USERNAME/.
rm -rf dotfiles/

